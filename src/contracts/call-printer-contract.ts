export interface CallPrinter {
    print(template: string, values: CallPrinter.Value): void
}

export namespace CallPrinter {
  export type Value = {
    [key: string]: string
  }
}
