import { HashTagTemplateParser } from '../provider/template-parser/implementation/hashtag-template-parser'
import { CallPrinterCupsService } from '../services/call-printer-cups'

const makeCallPrinterService = () => {
  const templateParser = new HashTagTemplateParser()
  const callPrinterService = new CallPrinterCupsService(templateParser)
  return callPrinterService
}

export { makeCallPrinterService }
