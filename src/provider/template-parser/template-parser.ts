export interface TemplateParser {
    parse(template: string, values: TemplateParser.Value): string
}

export namespace TemplateParser {
  export type Value = {
    [key: string]: string
  }
}
