import { TemplateParser } from '../provider/template-parser/template-parser'
import { CallPrinterCupsService } from './call-printer-cups'

interface SutTypes {
  sut: CallPrinterCupsService
  templateParserStub: TemplateParser
}

const makeTemplateParserStub = (): TemplateParser => {
  class TemplateParserStub implements TemplateParser {
    parse (template: string, values: TemplateParser.Value): string {
      return template
    }
  }

  return new TemplateParserStub()
}

const makeSut = ():SutTypes => {
  const templateParserStub = makeTemplateParserStub()
  const sut = new CallPrinterCupsService(templateParserStub)

  return {
    sut,
    templateParserStub
  }
}

describe('CallPrinterService', () => {
  test('Should be able to call templateParser with correct values', () => {
    const { sut, templateParserStub } = makeSut()
    const parseSpy = jest.spyOn(templateParserStub, 'parse')

    sut.print('any template', { any_key: 'any_values' })
    expect(parseSpy).toHaveBeenCalledWith('any template', { any_key: 'any_values' })
  })

  test('Should be able to call templateParser with correct values', () => {
    const { sut, templateParserStub } = makeSut()
    const parseSpy = jest.spyOn(templateParserStub, 'parse')

    sut.print('any template', { any_key: 'any_values' })
    expect(parseSpy).toHaveBeenCalledWith('any template', { any_key: 'any_values' })
  })

  test('Should be able to not return', () => {
    const { sut } = makeSut()

    const nullReponse = sut.print('any template', { any_key: 'any_values' })
    expect(nullReponse).toBeFalsy()
  })
})
